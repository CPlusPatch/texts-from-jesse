use matrix_sdk::{room::Joined, ruma::events::room::message::OriginalSyncRoomMessageEvent};
use serde_json::json;

/// Function for sending a reaction to a message in a chat room.
pub async fn react(room: &Joined, event: &OriginalSyncRoomMessageEvent, reaction: &str) {
    // Extract the event ID from the event
    let event_id = event.event_id.to_string();

    // Create a content for the reaction
    room.send_raw(json!({
        "m.relates_to": {
            "rel_type": "m.annotation",
            "event_id": event_id,
            "key": reaction,
        },
    }), "m.reaction", None).await.expect("Couldn't send reaction");


}