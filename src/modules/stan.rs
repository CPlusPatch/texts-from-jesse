use std::error::Error;

use matrix_sdk::{ruma::events::room::message::{OriginalSyncRoomMessageEvent, RoomMessageEventContent}, room::Joined};

pub async fn on_message(_event: &OriginalSyncRoomMessageEvent, room: &Joined, args: String) -> Result<(), Box<dyn Error>> {

    let content = RoomMessageEventContent::text_plain(format!("If {0} has a million fans, then I am one of them. If {0} has ten fans, then I am one of them. If {0} has only one fan then that is me. If {0} has no fans, then that means I am no longer on earth. If the world is against {0}, then I am against the world.", args));

   room.send(content, None).await.expect("Couldn't send joke");

    Ok(())
}