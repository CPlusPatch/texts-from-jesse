use std::{error::Error, fs};
use matrix_sdk::{ruma::events::room::message::{OriginalSyncRoomMessageEvent, MessageType, RoomMessageEventContent}, room::Joined, attachment::AttachmentConfig};
use serde::{Serialize, Deserialize};
use rand::Rng;
use rand::seq::SliceRandom;
use mime::Mime;

#[derive(Serialize, Deserialize)]
enum TriggerType {
    audio,
    text,
}

#[derive(Serialize, Deserialize)]
struct Trigger {
    trigger: String,
    responses: Vec<String>,
    probability: Option<f64>,
    trigger_type: Option<TriggerType>,
}

async fn check_responses_file_syntax() -> Result<bool, Box<dyn std::error::Error>> {
    let _triggers: Vec<Trigger> =
        serde_json::from_str(&fs::read_to_string("resources/responses.json").expect("Responses file not found")).expect("Couldn't parse responses file.");

    // Perform syntax checking on triggers if needed
    Ok(true)
}

pub async fn on_message(event: &OriginalSyncRoomMessageEvent, room: &Joined) -> Result<(), Box<dyn Error>> {
    check_responses_file_syntax().await.expect("Responses file is invalid or doesn't exist");

    let triggers: Vec<Trigger> =
        serde_json::from_str(&fs::read_to_string("resources/responses.json")?)?;

    let MessageType::Text(text_content) = &event.content.msgtype else { return Ok(()) };

    let body = &text_content.body;

    for trigger in triggers {
        if body.to_lowercase().contains(&trigger.trigger) {
            let probability = trigger.probability.unwrap_or(1.0);
            if rand::thread_rng().gen::<f64>() > probability {
                return Ok(());
            }

            let chosen_response = trigger
                .responses
                .choose(&mut rand::thread_rng())
                .unwrap_or(&"".to_string())
                .to_string();

            match trigger.trigger_type.unwrap_or(TriggerType::text) {
                TriggerType::text => {
                    room.send(RoomMessageEventContent::text_plain(&chosen_response), None).await?;
                }
                TriggerType::audio => {
                    // Send random audio file, path is at chosen_response
                    let resources_path = format!("resources/{}", chosen_response);
                   
                    let audio = fs::read(resources_path).expect("Can't open audio file.");

                    // Guess MIME type of file

                    let parts : Vec<&str> = chosen_response.split('.').collect();
                    let mut main_type = String::from("audio/");
                    main_type.push_str(parts.last().unwrap());

                    let mime_type: Mime = main_type.parse().unwrap();
                    room.send_attachment("+10 Social Credit!", &mime_type, &audio, AttachmentConfig::new()).await.unwrap();
                    
                }
            }
        }
    }
    Ok(())
}