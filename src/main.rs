use std::{path::Path, time::Duration};

use matrix_sdk::{room::Room, config::SyncSettings, ruma::events::room::{message::{MessageType, OriginalSyncRoomMessageEvent, RoomMessageEventContent}, member::StrippedRoomMemberEvent}, Client, Error, LoopCtrl};
use tokio::time::sleep;

mod login;
mod utils;
mod modules {
    pub mod joke;
    pub mod auto_response;
    pub mod stan;
    pub mod gender;
    pub mod nation;
}

/// A simple example to show how to persist a client's data to be able to
/// restore it.
///
/// Restoring a session with encryption without having a persisted store
/// will break the encryption setup and the client will not be able to send or
/// receive encrypted messages, hence the need to persist the session.
#[tokio::main]
async fn main() -> anyhow::Result<()> {
    // The folder containing this example's data.
    let data_dir = dirs::data_dir().expect("no data_dir directory found").join("texts-from-jesse");
    // The file where the session is persisted.
    let session_file = data_dir.join("session");

    let (client, sync_token) = if session_file.exists() {
        crate::login::restore_session(&session_file).await?
    } else {
        (crate::login::login(&data_dir, &session_file).await?, None)
    };

    sync(client, sync_token, &session_file).await.map_err(Into::into)
}

/// Setup the client to listen to new messages.
async fn sync(
    client: Client,
    initial_sync_token: Option<String>,
    session_file: &Path,
) -> anyhow::Result<()> {
    println!("Launching a first sync to ignore past messages…");

    let mut sync_settings = SyncSettings::default();

    // We restore the sync where we left.
    // This is not necessary when not using `sync_once`. The other sync methods get
    // the sync token from the store.
    if let Some(sync_token) = initial_sync_token {
        sync_settings = sync_settings.token(sync_token);
    }

    // Let's ignore messages before the program was launched.
    // This is a loop in case the initial sync is longer than our timeout. The
    // server should cache the response and it will ultimately take less time to
    // receive.
    loop {
        match client.sync_once(sync_settings.clone()).await {
            Ok(response) => {
                // This is the last time we need to provide this token, the sync method after
                // will handle it on its own.
                sync_settings = sync_settings.token(response.next_batch.clone());
                crate::login::persist_sync_token(session_file, response.next_batch).await?;
                break;
            }
            Err(error) => {
                println!("An error occurred during initial sync: {error}");
                println!("Trying again…");
            }
        }
    }

    println!("The client is ready! Listening to new messages…");

    // Now that we've synced, let's attach a handler for incoming room messages.
    client.add_event_handler(on_room_message);

    // Autojoin rooms
    client.add_event_handler(on_stripped_state_member);

    // This loops until we kill the program or an error happens.
    client
        .sync_with_result_callback(sync_settings, |sync_result| async move {
            let response = sync_result?;

            // We persist the token each time to be able to restore our session
            crate::login::persist_sync_token(session_file, response.next_batch)
                .await
                .map_err(|err| Error::UnknownError(err.into()))?;

            Ok(LoopCtrl::Continue)
        })
        .await?;

    Ok(())
}

async fn on_stripped_state_member(
    room_member: StrippedRoomMemberEvent,
    client: Client,
    room: Room,
    ) {
    if room_member.state_key != client.user_id().unwrap() {
        return;
    }

    if let Room::Invited(room) = room {
        tokio::spawn(async move {
            println!("Autojoining room {}", room.room_id());
            let mut delay = 2;

            while let Err(err) = room.accept_invitation().await {
                // retry autojoin due to synapse sending invites, before the
                // invited user can join for more information see
                // https://github.com/matrix-org/synapse/issues/4345
                eprintln!("Failed to join room {} ({err:?}), retrying in {delay}s", room.room_id());

                sleep(Duration::from_secs(delay)).await;
                delay *= 2;

                if delay > 3600 {
                    eprintln!("Can't join room {} ({err:?})", room.room_id());
                    break;
                }
            }
            println!("Successfully joined room {}", room.room_id());
        });
    }
}

/// Handle room messages.
async fn on_room_message(event: OriginalSyncRoomMessageEvent, room: Room, client: Client) -> Result<(), Box<dyn std::error::Error>> {
    // We only want to log text messages in joined rooms.
    let Room::Joined(room) = room else { return Ok(()) };
    let MessageType::Text(text_content) = &event.content.msgtype else { return Ok(()) };

    if event.sender == client.user_id().to_owned().unwrap() {
        return Ok(());
    }

	let body = &text_content.body;

	if body.starts_with("!j") {

        // Check if message length is too long
        if body.len() > 100 {
            // Send notice if message length is too long
			room.send(RoomMessageEventContent::text_plain("I'm not processing that, what do I look like, a fat kid at a Burger King? Make smaller inputs."), None).await.expect("Error sending message");
            return Ok(());
        }

        let command: Vec<&str> = body.split(" ").skip(1).collect(); // Split message body by space and remove first element ("!j") to get command
		match command.get(0).map(|&s| s) {
			Some("joke") => {
				// Get a new joke
    			crate::modules::joke::on_message(&event, &room).await?;
			},
            Some("stan") => {
                crate::modules::stan::on_message(&event, &room, (command[1..].join(" ")).to_string()).await?;
            },
            Some("gender") => {
                crate::modules::gender::on_message(&event, &room, command[1].to_string()).await?;
            },
            Some("nation") => {
                crate::modules::nation::on_message(&event, &room, command[1].to_string()).await?;
            },
			_ => {
                crate::utils::react(&room, &event, "❔").await;
            }
		}

    }

    crate::modules::auto_response::on_message(&event, &room).await?;

    Ok(())
}