use std::error::Error;

use matrix_sdk::{ruma::events::room::message::{OriginalSyncRoomMessageEvent, RoomMessageEventContent}, room::Joined};

pub async fn on_message(event: &OriginalSyncRoomMessageEvent, room: &Joined) -> Result<(), Box<dyn Error>> {
    crate::utils::react(&room, &event, "✅").await;

    let response = reqwest::get("https://v2.jokeapi.dev/joke/Any?format=json&type=single").await?;
    let json = response.json::<serde_json::Value>().await?;
    let joke = json.get("joke").expect("Couldn't parse JSON response").as_str();

    if let Some(joke) = joke {
        // Create a text message content with the joke as the body
        let content = RoomMessageEventContent::text_plain(joke);

        room.send(content, None).await.expect("Couldn't send joke");
    }

    Ok(())
}