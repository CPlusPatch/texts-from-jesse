use std::error::Error;

use matrix_sdk::{ruma::events::room::message::{OriginalSyncRoomMessageEvent, RoomMessageEventContent}, room::Joined};
use serde::Deserialize;

use capitalize::Capitalize;

#[derive(Deserialize)]
struct GenderResponse {
    gender: String,
    probability: f64,
}

pub async fn on_message(event: &OriginalSyncRoomMessageEvent, room: &Joined, args: String) -> Result<(), Box<dyn Error>> {
    crate::utils::react(&room, &event, "✅").await;

    let response = reqwest::get(format!("https://api.genderize.io?name={args}")).await?;
    let json = response.json::<GenderResponse>().await.ok();

    if let Some(json) = json {
        // Create a text message content with the joke as the body
        let content = RoomMessageEventContent::text_plain(format!("Gender is: {0} ({1}% probability)", json.gender.capitalize(), (json.probability * 100f64).ceil()));

        room.send(content, None).await.expect("Couldn't send joke");
    } else {
        crate::utils::react(&room, &event, "❌").await;

        let content = RoomMessageEventContent::text_plain("i shidded and farded :( (error running command)");
        room.send(content, None).await.expect("Couldn't send joke");
    }

    Ok(())
}