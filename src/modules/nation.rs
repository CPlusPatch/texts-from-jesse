use std::error::Error;

use matrix_sdk::{ruma::events::room::message::{OriginalSyncRoomMessageEvent, RoomMessageEventContent}, room::Joined};
use serde::Deserialize;

#[derive(Deserialize, Debug)]
struct NationResponse {
    country: Vec<NationProbability>
}

#[derive(Deserialize, Debug)]
struct NationProbability {
    country_id: String,
    probability: f64
}

pub async fn on_message(event: &OriginalSyncRoomMessageEvent, room: &Joined, args: String) -> Result<(), Box<dyn Error>> {
    crate::utils::react(&room, &event, "✅").await;

    let response = reqwest::get(format!("https://api.nationalize.io?name={args}")).await?;
    let json = response.json::<NationResponse>().await.ok();

    if let Some(mut json) = json {
        json.country.sort_unstable_by_key(|n| (n.probability * 10000f64).ceil() as i32);
        
        // Create a text message content with the joke as the body
        let content = RoomMessageEventContent::text_plain(format!("Nation is: {0} ({1}% probability)", json.country.last().unwrap().country_id, (json.country.last().unwrap().probability * 100f64).ceil()));

        room.send(content, None).await.expect("Couldn't send joke");
    } else {
        crate::utils::react(&room, &event, "❌").await;

        let content = RoomMessageEventContent::text_plain("i shidded and farded :( (error running command)");
        room.send(content, None).await.expect("Couldn't send joke");
    }

    Ok(())
}